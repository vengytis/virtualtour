package models;

import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.io.Serializable;

public class Waypoint implements Serializable {

    private String id;
    private String photoId;
    long position_x;
    long position_y;

    public Waypoint(String photoId, long position_x, long position_y) {
        this.photoId = photoId;
        this.position_x = position_x;
        this.position_y = position_y;
    }

    public String getPhoto() {
        return photoId;
    }

    public long getPosition_x() {
        return position_x;
    }

    public long getPosition_y() {
        return position_y;
    }

    public Waypoint (QueryDocumentSnapshot doc){
        this.id = doc.getId();
        this.photoId = (String) doc.getData().get("photo");
        this.position_x = (long) doc.getData().get("position_x");
        this.position_y = (long) doc.getData().get("position_y");
    }
}
