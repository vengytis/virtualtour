package models;

import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import venckus.gytis.virtualtour.AppManager;

public class Tour implements Serializable {
    private String id;
    private Map description;
    private String photo;
    private Map title;
    private List<Photo> photos;
    private int photosCount;

    public Tour(){

    }
    public Tour(Map description, String photo, Map title){
        this.description = description;
        this.photo = photo;
        this.title = title;
    }
    /**
     * Gets parsed data from FireStore "collection/document"
     * @param doc document from query
     */
    public Tour(QueryDocumentSnapshot doc){
        this.id = doc.getId();
        this.description = (Map) doc.getData().get("description");
        this.photo = doc.getData().get("photo").toString();
        this.title = (Map) doc.getData().get("title");
    }

    public String getDescription() {
        return description.get(AppManager.language).toString();
    }
    public Map getDescriptionMap(){
        return description;
    }
    public Map getTitleMap(){
        return title;
    }
    public void setPhotosList(List<Photo> photos){
        this.photos = photos;
    }
    public String getPhoto() {
        return photo;
    }
    public String getTitle() {
        return title.get(AppManager.language).toString();
    }
    public int getPhotosCount(){
        return this.photosCount;
    }
    public void setPhotosCount(int count){
        this.photosCount = count;
    }
    public List<Photo> getPhotosList(){
        return photos;
    }
    public String getId(){
        return this.id;
    }
    public void setId(String Id){
        this.id = Id;
    }
}
