package models;

import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import venckus.gytis.virtualtour.AppManager;

public class Photo implements Serializable {
    private String id;
    private String thumbnail;
    private Map title;
    private String photo;
    List<Waypoint> waypoints;

    public Photo(Map title, String photo) {
        this.title = title;
        this.photo = photo;
    }

    public Photo (QueryDocumentSnapshot doc){
        this.id = doc.getId();
        this.title = (Map) doc.getData().get("title");
        this.photo = doc.getData().get("photo").toString();
        this.thumbnail = doc.getData().get("thumbnail").toString();
    }

    public String getTitle() {
        return title.get(AppManager.language).toString();
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getPhoto() {
        return photo;
    }

    public String getPhotoName(){
        return this.photo.substring(photo.lastIndexOf('/') + 1, photo.length());
    }

    public String getPhotoNameShort(){
        String photo = getPhotoName().substring(0, getPhotoName().lastIndexOf('.'));
        return photo;
    }


    public void setWaypoints(List<Waypoint> waypoints){
        this.waypoints = waypoints;
    }
    public List<Waypoint> getWaypoints(){
        return waypoints;
    }

    public String getId(){
        return this.id;
    }
}
