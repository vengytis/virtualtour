/*
 * Copyright 2016 Gytis Venckus
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package venckus.gytis.virtualtour;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import models.Photo;
import models.Tour;

public class TourViewActivity extends AppCompatActivity {

    //Reference to model objects
    static Tour tour;
    static Photo pano;
    Fragment tourVrView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_view);
        getSupportActionBar().setTitle((AppManager.language == "lt") ? "Panorama" : "Panorama");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().hasExtra(AppManager.tourExtras)) {
            tour = (Tour) getIntent().getSerializableExtra(AppManager.tourExtras);
        }

        final ViewPager viewPager = findViewById(R.id.pager);
        final PagerAdapter adapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                tourVrView = new TourVRActivity();
                return tourVrView;
            }

            @Override
            public int getCount() {
                return 1;
            }
        };
        assert viewPager != null;
        viewPager.setAdapter(adapter);
    }
    public static Tour getTour(){
      return tour;
  }
    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }
}
