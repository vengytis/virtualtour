/*
 * Copyright 2016 Gytis Venckus
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package venckus.gytis.virtualtour;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.vr.sdk.widgets.pano.VrPanoramaView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;
import models.Photo;
import models.Tour;
import models.Waypoint;

/**
 * Fragment for handling the Welcome tab.
 */
public class TourVRActivity extends Fragment {

    private VrPanoramaView panoWidgetView;
    private TextView pano_title;
    Vibrator vibrator;
    private Activity activity;
    private boolean vibrated = false;
    private ProgressBar pbTimer;
    private ImageLoaderTask_local backgroundImageLoaderTask;
    private PanoLoaderTask_url panoImageLoaderTask;
    private CircleImageView crosshair;
    private int screenWidth;
    private int screenHeigth;
    private Tour tour;
    private Photo mainPanorama;
    private List<Waypoint> waypoints;
    private List<Photo> panoramas;
    private Map<Waypoint, CircleImageView> waypointViews;
    private Map<Waypoint, TextView> waypointTextViews;
    private long timePassed = 0;
    //panoSwitchTimer in micro seconds for display accuracy
    private int panoSwitchTimer = 2 * 100000;

    private float bounds = 10f;
    float [] headRotation = new float[2];
    private final Handler handler = new Handler();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.activity_tour_vr, container, false);
        initViews(v);
        activity = getActivity();
        screenWidth = this.getResources().getDisplayMetrics().widthPixels;
        screenHeigth = this.getResources().getDisplayMetrics().heightPixels;
        tour = TourViewActivity.getTour();
        panoramas = getPanoramas();
        mainPanorama = defaultPano();
        pano_title.setText(mainPanorama.getTitle());
        waypoints = getWaypoints(mainPanorama);
        waypointViews = createWaypointViews(waypoints);
        waypointTextViews = createWaypointText(waypoints);
        pbTimer.setVisibility(v.INVISIBLE);
        pbTimer.setMax(panoSwitchTimer);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadPanoImageFromUrl(mainPanorama.getPhoto());
        panoWidgetView.setFullscreenButtonEnabled(false);
        panoWidgetView.setStereoModeButtonEnabled(false);
        panoWidgetView.setInfoButtonEnabled(false);
        vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        System.out.println(AppManager.isOnline(getContext()));
        trackHead();
    }


    @Override
    public void onPause() {
        panoWidgetView.pauseRendering();
        super.onPause();
    }

    @Override
    public void onResume() {
        panoWidgetView.resumeRendering();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        // Destroy the widget and free memory.
        panoWidgetView.shutdown();
        super.onDestroy();
    }

    private void trackHead() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                panoWidgetView.getHeadRotation(headRotation);
                displayWaypoints(waypointViews, waypointTextViews);
                Waypoint closestWaypoint = getClosestWaypoint(waypoints, headRotation);
                    if (centerInWaypointBounds(headRotation, closestWaypoint)){
                        if (!vibrated) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                vibrator.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                            } else {
                                vibrator.vibrate(500);
                            }
                            vibrated = true;
                        }
                        timePassed += System.currentTimeMillis() / 1000000000;
                        pbTimer.setElevation(10);
                        pbTimer.setVisibility(View.VISIBLE);
                        pbTimer.setProgress((int) timePassed);
                        if (pbTimer.getProgress() >= panoSwitchTimer) {
                            clearViews();
                            mainPanorama = getPanoramaById(closestWaypoint.getPhoto());
                            changePanoramaLogic(mainPanorama);
                            timePassed = 0;
                        }
                    } else {
                        vibrated = false;
                        timePassed = 0;
                        pbTimer.setVisibility(View.INVISIBLE);
                }
                trackHead();
            }
        }, 1);
    }

    private synchronized void loadPanoImageLocal(String panoImageName) {
        ImageLoaderTask_local task = backgroundImageLoaderTask;
        if (task != null && !task.isCancelled()) {
            // Cancel any task from a previous loading.
            task.cancel(true);
        }

        // pass in the name of the image to load from assets.
        VrPanoramaView.Options viewOptions = new VrPanoramaView.Options();
        viewOptions.inputType = VrPanoramaView.Options.TYPE_MONO;

        // use the name of the image in the assets/ directory.
        // create the task passing the widget view and call execute to start.
        task = new ImageLoaderTask_local(panoWidgetView, viewOptions, panoImageName);
        task.execute(getActivity().getAssets());
        backgroundImageLoaderTask = task;
    }

    private synchronized void loadPanoImageFromUrl(String panoUrl) {
        PanoLoaderTask_url task_url = panoImageLoaderTask;
        if (task_url != null && !task_url.isCancelled()) {
            // Cancel any task from a previous loading.
            task_url.cancel(true);
        }

        // pass in the name of the image to load from assets.
        VrPanoramaView.Options viewOptions = new VrPanoramaView.Options();
        viewOptions.inputType = VrPanoramaView.Options.TYPE_MONO;

        // use the name of the image in the assets/ directory.
        // create the task passing the widget view and call execute to start.
        task_url = new PanoLoaderTask_url(panoWidgetView, viewOptions, panoUrl);
        task_url.execute(panoUrl);
        panoImageLoaderTask = task_url;
    }

    private void initViews(View v) {
        panoWidgetView = v.findViewById(R.id.pano_view);
        pbTimer = v.findViewById(R.id.progressBar);
        pano_title = v.findViewById(R.id.pano_title);
        crosshair = v.findViewById(R.id.crosshair);
    }

    private Photo defaultPano(){
        for (Photo pano : tour.getPhotosList()) {
                mainPanorama = pano;
            }
        return mainPanorama;
    }

    private Photo getPanoramaById(String panoId){
        Photo panorama = null;
        for (Photo pano : panoramas) {
            if (pano.getId().equals(panoId)) {
                panorama = pano;
            }
        }
        return panorama;
    }

    public List<Photo> getPanoramas(){
        return tour.getPhotosList();
    }
    private List<Waypoint> getWaypoints(Photo currentPanorama){
        return currentPanorama.getWaypoints();
    }

    private Waypoint getClosestWaypoint(List<Waypoint> waypoints, float [] headRotation){
        Waypoint closestWaypoint = null;
        float min = Float.MAX_VALUE;
        for (Waypoint waypoint : waypoints){
            if (Math.abs((waypoint.getPosition_x() - headRotation[0]) + waypoint.getPosition_y() - headRotation[1]) < min){
                min = Math.abs((waypoint.getPosition_x() - headRotation[0]) + waypoint.getPosition_y() - headRotation[1]);
                closestWaypoint = waypoint;
            }
        }
        return closestWaypoint;
    }

    private void displayWaypoints(Map<Waypoint, CircleImageView> waypointViews,  Map<Waypoint, TextView> waypointTextViews){
        Set<Waypoint> keySet = waypointViews.keySet();
        for (Waypoint waypoint : keySet){
            waypointViews.get(waypoint).setX((-headRotation[0] + waypoint.getPosition_x()) * 9 + panoWidgetView.getWidth() / 2 - waypointViews.get(waypoint).getWidth() / 2);
            waypointViews.get(waypoint).setY((headRotation[1] - waypoint.getPosition_y()) * 9 + panoWidgetView.getHeight() / 2 - waypointViews.get(waypoint).getHeight() / 2);
            waypointTextViews.get(waypoint).setX(waypointViews.get(waypoint).getX() - waypointTextViews.get(waypoint).getWidth() / 2 + waypointViews.get(waypoint).getWidth() / 2);
            waypointTextViews.get(waypoint).setY(waypointViews.get(waypoint).getY() + waypointViews.get(waypoint).getHeight());
        }
    }

    private Map<Waypoint, CircleImageView> createWaypointViews(List<Waypoint> waypoints){
        Map<Waypoint, CircleImageView> waypointViews = new HashMap<>();
        if (isAdded() && activity != null) {
            for (Waypoint waypoint : waypoints) {
                CircleImageView waypointView = new CircleImageView(getContext());
                waypointView.setLayoutParams(new LinearLayout.LayoutParams(screenWidth / 6, screenWidth / 6));
                waypointView.setBorderColor(Color.BLACK);
                waypointView.setBorderWidth(3);
                new ImageLoaderTask_url(waypointView).execute(getPanoramaById(waypoint.getPhoto()).getThumbnail());
                waypointViews.put(waypoint, waypointView);
                panoWidgetView.addView(waypointView);
            }
        }
        return waypointViews;
    }

    private Map<Waypoint, TextView> createWaypointText(List<Waypoint> waypoints){
        Map<Waypoint, TextView> waypointTextViews = new HashMap<>();
        if (isAdded() && activity != null) {
            for (Waypoint waypoint : waypoints) {
                TextView waypointText = new TextView(getContext());
                waypointText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                waypointText.setText(getPanoramaById(waypoint.getPhoto()).getTitle());
                waypointText.setTypeface(waypointText.getTypeface(), Typeface.BOLD);
                waypointText.setTextColor(Color.WHITE);
                waypointTextViews.put(waypoint, waypointText);
                panoWidgetView.addView(waypointText);
            }
        }
        return waypointTextViews;
    }


    private void changePanoramaLogic(Photo currentPano){
        loadPanoImageFromUrl(currentPano.getPhoto());
        pano_title.setText(currentPano.getTitle());
        waypoints = currentPano.getWaypoints();
        waypointViews = createWaypointViews(waypoints);
        waypointTextViews = createWaypointText(waypoints);
    }

    private void clearViews(){
        Iterator<Waypoint> pano_iterator = waypointViews.keySet().iterator();
        while (pano_iterator.hasNext()){
            Waypoint waypoint = pano_iterator.next();
            panoWidgetView.removeView(waypointViews.get(waypoint));
            pano_iterator.remove();
        }
        Iterator<Waypoint> text_iterator = waypointTextViews.keySet().iterator();
        while (text_iterator.hasNext()){
            Waypoint waypoint = text_iterator.next();
            panoWidgetView.removeView(waypointTextViews.get(waypoint));
            text_iterator.remove();
        }
    }

    private boolean centerInWaypointBounds(float [] headRotation, Waypoint waypoint){
        if (headRotation[0] >= waypoint.getPosition_x() - bounds &&
                headRotation[0] <= waypoint.getPosition_x() + bounds &&
                headRotation[1] >= waypoint.getPosition_y() - bounds &&
                headRotation[1] <= waypoint.getPosition_y() + bounds){
            return true;
        }
        return false;
    }
}

