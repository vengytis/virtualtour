package venckus.gytis.virtualtour;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import models.Tour;

public class AddNewTour extends AppCompatActivity {

    private TextView titleText;
    private EditText tourTitle_lt;
    private EditText tourDescription_lt;
    private EditText tourTitle_en;
    private EditText tourDescription_en;
    private ImageView tourThumbnail;
    private TextView tourThumbnailTitle;
    private Button submitTourBtn;
    private Tour newTour;

    private FirestoreManager firestoreManager;

    public static final int GALLERY_REQUEST_CODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_tour);
        getSupportActionBar().setTitle((AppManager.language == "lt") ? "Pridėti naują turą (demo)" : "Add new tour (demo)");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        InitializeViews();
        SetInitialTexts();
        firestoreManager = new FirestoreManager();
        tourThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFromGallery();
            }
        });
        setSubmitTourBtn();
    }

    private void InitializeViews(){
        tourTitle_lt = findViewById(R.id.tourTitle_lt);
        tourDescription_lt = findViewById(R.id.tourDescription_lt);
        tourTitle_en = findViewById(R.id.tourTitle_en);
        tourDescription_en = findViewById(R.id.tourDescription_en);
        tourThumbnail = findViewById(R.id.tourThumbnail);
        tourThumbnailTitle = findViewById(R.id.tourThumbnailTitle);
        submitTourBtn = findViewById(R.id.submitTourBtn);
    }

    private void SetInitialTexts(){
        submitTourBtn.setText((AppManager.language == "lt") ? "Sukurti turą" : "Create tour");
        tourTitle_lt.setHint((AppManager.language == "lt") ? "Turo pavadinimas (LT kalba)" : "Tour title (LT language)");
        tourDescription_lt.setHint((AppManager.language == "lt") ? "Turo aprašymas (LT kalba)" : "Tour description (LT language)");
        tourTitle_en.setHint((AppManager.language == "lt") ? "Turo pavadinimas (EN kalba)" : "Tour title (EN language)");
        tourDescription_en.setHint((AppManager.language == "lt") ? "Turo aprašymas (EN kalba)" : "Tour description (EN language)");
        tourThumbnailTitle.setText((AppManager.language == "lt") ? "Pasirinkite paveiksliuką" : "Select picture");
    }

    private void pickFromGallery(){
        Intent intent=new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
        startActivityForResult(intent, GALLERY_REQUEST_CODE);

    }

    public void onActivityResult(int requestCode,int resultCode,Intent data){
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode){
                case GALLERY_REQUEST_CODE:
                    Uri selectedImage = data.getData();
                    tourThumbnail.setImageURI(selectedImage);
                    break;
            }
    }

    public void setSubmitTourBtn(){
        submitTourBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map description = new HashMap(){
                    {
                        put("lt", tourDescription_lt.getText().toString());
                        put("en", tourDescription_en.getText().toString());
                    }
                };
                Map title = new HashMap(){
                    {
                        put("lt", tourTitle_lt.getText().toString());
                        put("en", tourTitle_en.getText().toString());
                    }
                };
                if (newTourValidation()) {
                    newTour = new Tour(description, "https://g1.dcdn.lt/images/pix/anyksciai-61497848.jpg", title);
                    firestoreManager.addNewTour(newTour);
                    Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivityForResult(myIntent, 0);
                }
            }
        });
    }

    private boolean FieldsValidation(EditText field1, EditText field2, String errorMsg){
        if (field1.length() <= 0 && field2.length() <= 0){
            Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean newTourValidation(){
        if (!FieldsValidation(tourTitle_lt, tourTitle_lt, "Tour title is not set!"))
            return false;
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }
}
