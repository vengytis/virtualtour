package venckus.gytis.virtualtour;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Photo;
import models.Tour;
import models.Waypoint;

public class FirestoreManager {


    private String Tour_TAG = this.getClass().getSimpleName() + " Tours query ";
    private String Photos_TAG = this.getClass().getSimpleName() + " Photos query ";
    private String Waypoints_TAG = this.getClass().getSimpleName() + " Waypoints query ";

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    private List<Tour> tours = new ArrayList<>();
    private List<Photo> photos = new ArrayList<>();
    private List<Waypoint> waypoints = new ArrayList<>();

    public List<Tour> getTours() {
        return tours;
    }

    public FirestoreManager(){
        getToursList();
    }

    private void getToursList(){
        final CollectionReference toursRef = db.collection("virtual_tours");
        toursRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot doc : task.getResult()) {
                        final Tour tour = new Tour(doc);
                        tours.add(tour);
                        getPhotosList(toursRef, tour);
                        Log.d(Tour_TAG, doc.getId() + " => " + doc.getData());
                    }
                    MainActivity.notifyAdapter();
                } else {
                    Log.d(Tour_TAG, "Error getting documents ", task.getException());
                }
            }
        });
    }
    private void getPhotosList(CollectionReference toursRef, final Tour tour){
        final CollectionReference photosRef = db.collection(toursRef.getPath()).document(tour.getId()).collection("photos");
        photosRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                photos = new ArrayList<>();
                if (task.isSuccessful()) {
                    Log.d(Photos_TAG, "Task successful");
                    for (QueryDocumentSnapshot doc : task.getResult()) {
                        Photo pano = new Photo(doc);
                        photos.add(pano);
                        getWaypointsList(photosRef, pano);
                        Log.d(Photos_TAG, doc.getId() + " => " + doc.getData());
                    }
                    tour.setPhotosList(photos);
                    tour.setPhotosCount(photos.size());
                    MainActivity.notifyAdapter();
                } else {
                    Log.d(Photos_TAG, "Error getting documents ", task.getException());
                }
            }
        });
    }

    private void getWaypointsList(CollectionReference photosRef, final Photo pano){
        CollectionReference waypointsRef = db.collection(photosRef.getPath()).document(pano.getId()).collection("waypoints");
        waypointsRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                waypoints = new ArrayList<>();
                if (task.isSuccessful()) {
                    Log.d(Waypoints_TAG, "Task successful");
                    for (QueryDocumentSnapshot doc : task.getResult()) {
                        waypoints.add(new Waypoint(doc));
                        Log.d(Waypoints_TAG, doc.getId() + " => " + doc.getData());
                    }
                    pano.setWaypoints(waypoints);
                    MainActivity.notifyAdapter();
                } else {
                    Log.d(Waypoints_TAG, "Error getting documents ", task.getException());
                }
            }
        });
    }

    public void addNewTour(Tour newTour){
        Map<String, Object> data = new HashMap<>();
        data.put("description", newTour.getDescriptionMap());
        data.put("title", newTour.getTitleMap());
        data.put("photo", newTour.getPhoto());

        db.collection("virtual_tours")
                .add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("ADD NEW TOUR", "DocumentSnapshot written with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("ADD NEW TOUR", "Error adding document", e);
                    }
                });
    }
}
