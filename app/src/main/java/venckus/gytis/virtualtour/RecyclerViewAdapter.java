package venckus.gytis.virtualtour;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import models.Tour;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    public interface OnItemClickListener{
        void onItemClick(Tour tour);
        }

    private List<Tour> tours;
    private final OnItemClickListener listener;

    public RecyclerViewAdapter(List<Tour> tours, OnItemClickListener listener){
        this.tours = tours;
        this.listener = listener;
    }

    //todo should be static class
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView tourImage;
        public TextView tourTitle;
        public TextView photosCount;
        public TextView tourDescription;

        public MyViewHolder(View view) {
            super(view);
            tourImage = view.findViewById(R.id.tourImage);
            tourTitle = view.findViewById(R.id.tourTitle);
            photosCount = view.findViewById(R.id.photosCount);
            tourDescription = view.findViewById(R.id.tourDescription);
        }


        public void bind(final Tour tour, final OnItemClickListener listener) {
            new ImageLoaderTask_url(tourImage).execute(tour.getPhoto());
            tourTitle.setText(tour.getTitle());
            photosCount.setText("Panoramų skaičius: " + tour.getPhotosCount());
            tourDescription.setText(tour.getDescription());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(tour);
                }
            });
        }
    }

    @Override
    public RecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tours_listview, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(tours.get(position), listener);
    }


    @Override
    public int getItemCount(){
        return tours.size();
    }



}
