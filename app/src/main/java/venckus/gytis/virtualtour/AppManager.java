package venckus.gytis.virtualtour;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class AppManager {

    public static String language;
    public static String tourExtras = "TOUR";

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
}
