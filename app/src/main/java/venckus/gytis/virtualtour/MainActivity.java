package venckus.gytis.virtualtour;

import android.content.Intent;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import models.Tour;

public class MainActivity extends AppCompatActivity {

    private TextView listTitle;
    private FloatingActionButton addNewTourButton;
    private RecyclerView recyclerView;
    private static RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private File directory = new File(Environment.getExternalStorageDirectory() + "/tours/");
    private FirestoreManager firestoreManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle((AppManager.language == "lt") ? "Virtualūs turai" : "Virtual tours");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addNewTourButton = findViewById(R.id.newTourButton);
        addNewTour();
        firestoreManager = new FirestoreManager();
        if (!directory.exists()) {
            directory.mkdir();
            System.out.println("directory exists: " + directory.exists());
        }
        InitRecyclerView();
        AdapterLogic(firestoreManager.getTours());
        recyclerView.setAdapter(mAdapter);
    }

    private void InitRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void AdapterLogic(List<Tour> tours){
        mAdapter = new RecyclerViewAdapter(tours, new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Tour tour) {
                Toast message;
                if (tour.getPhotosCount() <= 0) {
                    message = Toast.makeText(getApplicationContext(), "This tour has no panoramas!", Toast.LENGTH_SHORT);
                } else {
                    message = Toast.makeText(getApplicationContext(), tour.getTitle(), Toast.LENGTH_SHORT);
                    Intent openTour = new Intent(getApplicationContext(), TourViewActivity.class);
                    openTour.putExtra(AppManager.tourExtras, tour);
                    startActivity(openTour);
                }
                message.show();
            }
        });
    }

    public static void notifyAdapter(){
        mAdapter.notifyDataSetChanged();
    }

    public void addNewTour(){
        addNewTourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(getApplicationContext(), AddNewTour.class);
                startActivity(intent);
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        Intent myIntent = new Intent(getApplicationContext(), StartingActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }
}
